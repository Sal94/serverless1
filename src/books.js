// 1. Modify the frontend and the Lambda function so it displays all the books in the database in the table
//    at the bottom of the page.
// 2. Modify the frontend and the Lambda function so that books can be deleted.
// 3. Implement a separate database and frontend page for authors. Create fields for author id (aid),
//    first name (fname), and last name (lname).
// 4. Modify the books page so that there is a new field for the author of the book. When entering a new book,
//    show a pick list for author based on the contents of the authors database. The books table should store
//    author id (aid), but the frontend should show the corresponding author’s first and last name.

import React, { Component } from 'react';
import axios from 'axios';

// UPDATE WITH YOUR API ENDPOINT
const baseURL ="https://gk1m6nmjvh.execute-api.us-east-1.amazonaws.com/default/library";

class Books extends Component {
  
  constructor(props){
    super(props);
    this.state = {
      books:[],
      authors:[]
    };
    this.getAuthorFromID = this.getAuthorFromID.bind(this);
  }
  
  componentDidMount(){
    axios.get( baseURL + "/books" )
    .then(res=>{
      console.log(res.data);
      this.setState({
        books: res.data.Items
      });
    })
    .catch(error=>console.log(error));
    
    axios.get( baseURL + "/authors")
    .then(res=>{
      console.log(res.data);
      this.setState({
        authors: res.data.Items
      });
    })
    .catch(error=>console.log(error));
    
  }

  getAuthorFromID = (aid) => { 
    for(let i = 0; i < this.state.authors.length; i++){
      if(parseInt(this.state.authors[i].aid) === parseInt(aid)){
        return this.state.authors[i].fname + " " + this.state.authors[i].lname;
      }
    }
    return "Author N/A"
  }
  
  addBook(ev) {
    const inputISBN = ev.target.querySelector('[id="isbn"]');
    const isbn = inputISBN.value.trim();

    const inputTitle = ev.target.querySelector('[id="title"]');
    const title = inputTitle.value.trim();

    const inputPages = ev.target.querySelector('[id="pages"]');
    const pages = inputPages.value.trim();
    
    const inputAID = ev.target.querySelector('[id="aid"]');
    const aid = inputAID.value.trim();


    console.log( "ISBN: " + isbn );
    console.log( "Title: " + title );
    console.log( "Pages: " + pages );
    console.log( "Author: " + aid );
    
    let newbook = {
      isbn,
      title,
      pages,
      aid
    };
    
    axios.post( baseURL + "/books", newbook)
    .then(res => {
      console.log('added')
    });
    
    ev.preventDefault();
  }
  
  
  deleteBook(isbn, ev) {
    console.log(isbn)
    axios.delete(baseURL + "/books/" + isbn)
    .then(result => {
      console.log('removed')
    })
    .catch(err=>console.log(err));
    
    ev.preventDefault();
  }

    
  render() {
    return(
    <div>
      <div className="py-5 text-center">
        <h2>Books</h2>
      </div>

      <div className="row">
        <div className="col-md-12 order-md-1">
          <form onSubmit={this.addBook.bind(this)} className="needs-validation" noValidate>
            <div className="row">
              <div className="col-md-2 mb-3">
                <label htmlFor="isbn">ISBN</label>
                <input type="number" className="form-control" id="isbn" defaultValue="1" required />
                <div className="invalid-feedback">
                    An ISBN is required.
                </div>
              </div>
              
              <div className="col-md-4 mb-3">
              <label htmlFor="author">Author</label>
              <select id="aid" className="form-control">
                {this.state.authors.map(author=>{
                  return <option value={author.aid} key={author.aid}>{author.fname + " "+ author.lname}</option>;
                })}
              </select>
              </div>


              <div className="col-md-8 mb-3">
                <label htmlFor="title">Title</label>
                <input type="text" className="form-control" id="title" defaultValue="" required />
                <div className="invalid-feedback">
                    A book title is required.
                </div>
              </div>

              <div className="col-md-2 mb-3">
                <label htmlFor="pages">Pages</label>
                <input type="number" className="form-control" id="pages" defaultValue="100" required />
                <div className="invalid-feedback">
                    The number of pages is required.
                </div>
              </div>

            </div>
            <button className="btn btn-primary btn-lg btn-block" type="submit">Add book</button>
          </form>
        </div>
      </div>
      
      <table className="table">
        <thead>
          <tr>
            <th scope="col">ISBN</th>
            <th scope="col">Title</th>
            <th scope="col">Pages</th>
            <th scop ="col">AuthorID</th> 
            <th scope="col">Delete</th>
          </tr>
        </thead>
        
        <tbody>
          {this.state.books && this.state.books.map(book => <tr key={book.isbn}>
            <th scope="row">{book.isbn}</th>
            <td>{book.title}</td>
            <td>{book.pages}</td>
            <td>{this.getAuthorFromID(book.aid)}</td>
            <td><button onClick={this.deleteBook.bind(this, book.isbn)} type="button" className="btn btn-danger">Delete</button></td>
          </tr>)}

        </tbody>
        
        
      </table>

      <footer className="my-5 pt-5 text-muted text-center text-small">
        <p className="mb-1">&copy; 2020 CPSC 2650</p>
      </footer>
    </div>
    );
  }
}

export default Books;
