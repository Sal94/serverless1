import React, { Component } from 'react';

import axios from 'axios';

// UPDATE WITH YOUR API ENDPOINT
const baseURL ="https://gk1m6nmjvh.execute-api.us-east-1.amazonaws.com/default/library";

class Authors extends Component {
  
  constructor(props){
    super(props);
    this.state = {
      authors:[]
    };
  }
  
  componentDidMount(){
    axios.get( baseURL + "/authors" )
    .then(result=>{
      this.setState({
        authors: result.data.Items
      });
    })
    .catch(error=>console.log(error));
  }

  
  addAuthor(ev) {
    const inputAID = ev.target.querySelector('[id="aid"]');
    const aid = inputAID.value.trim();

    const inputFname = ev.target.querySelector('[id="fname"]');
    const fname = inputFname.value.trim();

    const inputLname = ev.target.querySelector('[id="lname"]');
    const lname = inputLname.value.trim();

    console.log( "aid: " + aid );
    console.log( "fname: " + fname );
    console.log( "lname: " + lname );
    
    
    let newAuthor = {
      aid,
      fname,
      lname
    };
    
    axios.post( baseURL + "/authors", newAuthor)
    .then(res => {
      console.log(res);
    });
    
    ev.preventDefault();
  }
  
  render() {

    return(
    <div>
      <div className="py-5 text-center">
        <h2>Authors</h2>
      </div>

      <div className="row">
        <div className="col-md-12 order-md-1">
          <form onSubmit={this.addAuthor.bind(this)} className="needs-validation" noValidate>
            <div className="row">
              <div className="col-md-2 mb-3">
                <label htmlFor="isbn">Author ID</label>
                <input type="number" className="form-control" id="aid" defaultValue="1" required />
                <div className="invalid-feedback">
                    An Author ID is required.
                </div>
              </div>

              <div className="col-md-5 mb-3">
                <label htmlFor="title">First Name</label>
                <input type="text" className="form-control" id="fname" defaultValue="" required />
                <div className="invalid-feedback">
                    An Author First Name is required.
                </div>
              </div>

              <div className="col-md-5 mb-3">
                <label htmlFor="pages">Last Name</label>
                <input type="text" className="form-control" id="lname" defaultValue="" required />
                <div className="invalid-feedback">
                    An Author Last Name is required.
                </div>
              </div>

            </div>
            <button className="btn btn-primary btn-lg btn-block" type="submit">Add Author</button>
          </form>
        </div>
      </div>
      
      <table className="table">
        <thead>
          <tr>
            <th scope="col">Author ID</th>
            <th scope="col">First Name</th>
            <th scope="col">Last Names</th>
          </tr>
        </thead>
        <tbody>
          
        {this.state.authors && this.state.authors.map(author=>
          <tr key={author.aid}>
          <td>{author.aid}</td>
          <td>{author.fname}</td>
          <td>{author.lname}</td>
          
          </tr>)}
        
        </tbody>
      </table>
      
      <footer className="my-5 pt-5 text-muted text-center text-small">
        <p className="mb-1">&copy; 2020 CPSC 2650</p>
      </footer>
    </div>
    );
  }
}

export default Authors;
